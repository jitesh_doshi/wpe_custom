<?php

namespace Drupal\wpe_custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'HourMinute_default' formatter.
 *
 * @FieldFormatter(
 *   id = "HourMinute_default",
 *   label = @Translation("Hour Minute"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class HourMinuteFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    $summary[] = t('Displays integer minutes as HH hr MI min.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = array();

    foreach ($items as $delta => $item) {
      $hours = floor($item->value/60);
      $minutes = $item->value % 60;
      // Render each element as markup.
      $element[$delta] = array(
        '#type' => 'markup',
        '#markup' => ($hours ? "$hours hr " : '') . ($minutes ? "$minutes m" : '')
      );
    }

    return $element;
  }

}

