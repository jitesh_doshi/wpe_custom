<?php

namespace Drupal\wpe_custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'IMDB_default' formatter.
 *
 * @FieldFormatter(
 *   id = "IMDB_default",
 *   label = @Translation("IMDB Link"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class IMDBFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    $summary[] = t('Displays plain text as IMDB Link.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = array();

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      if(strpos($item->value, 'tt') === 0) {
        $element[$delta] = array(
          '#type' => 'markup',
          '#markup' => "<a href='http://www.imdb.com/title/$item->value' target='_blank'>IMDB Link</a>",
        );
      } else if(strpos($item->value, 'nm') === 0) {
        $element[$delta] = array(
          '#type' => 'markup',
          '#markup' => "<a href='http://www.imdb.com/name/$item->value' target='_blank'>IMDB Link</a>",
        );
      } else {
        $element[$delta] = array(
          '#type' => 'markup',
          '#markup' => "<span>$item->value</span>",
        );
      }
    }

    return $element;
  }

}

