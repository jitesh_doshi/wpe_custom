// Immediately Invoked Anonymous Function
(function($) {
Drupal.behaviors.imdb_lookup = {
  attach: function(context, settings) {
    var div = $('form.node-movie-edit-form #edit-field-imdb-id-wrapper > div', context);
    if(!div.size()) return;
    var handle = $('<a href="#">Lookup IMDB ID by Title</a>');
    div.append(handle);
    var input = div.find('input');
    var title_input = $('#edit-title-0-value', context);
    handle.click(function() {
      console.log('val', title_input.val());
      $.get('http://www.omdbapi.com/', {t: title_input.val(), tomatoes: true}, function(data) {
        console.log('data', data);
	if(data && data.imdbID) {
	  input.val(data.imdbID);
	}
	if(data && data.Error) {
	  alert(data.Error);
	}
      });
      return false;
    });
  }
}
})(jQuery);
